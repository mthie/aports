# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=graphviz
pkgver=5.0.0
pkgrel=1
pkgdesc="Graph Visualization Tools"
url="https://www.graphviz.org/"
arch="all"
license="EPL-1.0"
_luaver=5.4
depends_dev="
	cairo-dev
	expat-dev
	fontconfig-dev
	freetype-dev
	gmp-dev
	libjpeg-turbo-dev
	libpng-dev
	libsm-dev
	libxext-dev
	pango-dev
	python3-dev
	zlib-dev
	"
makedepends="
	$depends_dev
	autoconf
	automake
	bash
	bison
	flex
	libltdl
	libtool
	libxaw-dev
	lua$_luaver-dev
	m4
	swig
	tcl
	"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/graphviz"
subpackages="$pkgname-dev $pkgname-doc py3-gv:_py3 lua$_luaver-$pkgname:_lua
	$pkgname-graphs::noarch"
source="https://gitlab.com/graphviz/graphviz/-/archive/$pkgver/graphviz-$pkgver.tar.gz
	"

# secfixes:
#   2.46.0-r0:
#     - CVE-2020-18032

prepare() {
	default_prepare
	NOCONFIGURE=1 ./autogen.sh
}

build() {
	CONFIG_SHELL=/bin/bash \
	LIBPOSTFIX=/ \
	LUA=lua$_luaver \
	lua_suffix=$_luaver \
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-python \
		--disable-silent-rules \
		--disable-static \
		--disable-dependency-tracking \
		--disable-ltdl-install \
		--enable-ltdl \
		--enable-sharp=no \
		--enable-go=no \
		--enable-guile=no \
		--enable-java=no \
		--enable-lua=yes \
		--enable-ocaml=no \
		--enable-perl=no \
		--enable-php=no \
		--enable-python3=yes \
		--enable-r=no \
		--enable-ruby=no \
		--enable-tcl=no \
		--without-included-ltdl \
		--with-x \
		--with-rsvg=yes \
		--with-pangocairo=yes \
		--with-gdk-pixbuf=yes \
		--with-libgd=no \
		--with-ipsepcola=yes

	if [ "$CARCH" = "x86_64" ]; then
		# the configure script thinks we have sincos. we dont.
		sed -i -e '/HAVE_SINCOS/d' config.h
	fi

	make
}

package() {
	# Install has race conditions...
	make -j1 DESTDIR="$pkgdir" \
		pkgconfigdir=/usr/lib/pkgconfig \
		install
}

_py3() {
	pkgdesc="Python3 extension for graphviz"
	depends="python3"

	amove usr/lib/graphviz/python3* \
		usr/lib/python3*
}

_lua() {
	pkgdesc="Lua$_luaver extension for graphviz"
	provides="lua-$pkgname=$pkgver-r$pkgrel"  # for backward compatibility
	replaces="lua-$pkgname"  # for backward compatibility

	mkdir -p "$subpkgdir"/usr/lib/lua
	mv "$pkgdir"/usr/lib/lua "$subpkgdir"/usr/lib/lua/$_luaver
	amove usr/lib/graphviz/lua
}

graphs() {
	pkgdesc="Demo graphs for graphviz"

	amove usr/share/graphviz/graphs
}

sha512sums="
26994d07e34878ad309b0796d2bde2b1c86a04cbe6d80a218c8d9afdeee2aabad3b645b0cbbc7bea8bcbc09c0a402838fa5ebac985117275f5433144379ddde2  graphviz-5.0.0.tar.gz
"
